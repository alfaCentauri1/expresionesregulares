# ExpresionesRegulares

Ejemplo del uso de expresiones regulares en Java.

## Requisitos

* JDK 18 ó superior.

***

## Instalación
* Compile el proyecto con el IDE de su preferencia.
* Ejecute desde la consola; en la raíz del proyecto, java Ejecutable.jar

## Authors and acknowledgment
[GitLab: alfaCentauri1](https://gitlab.com/alfaCentauri1)

## License
GNU version 3.

## Project status
* Developer